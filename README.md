## Run application:

```bash
yarn
yarn start
```

## Build code for development:

```bash
yarn build-dev
```

## Build code for production:

```bash
yarn build-prod
```

## Run tests:

```bash
yarn test
```

## Run tests in watch mode:

```bash
yarn test:watch
```

## Create test coverage:

```bash
yarn test:coverage
```

import React, { lazy, Suspense } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import ModalRoot from './containers/modal'
const HomeContainer = lazy(() => import('../src/containers/home'))
const SettingsContainer = lazy(() => import('../src/containers/settings'))

const App = () => {
	return (
		<BrowserRouter>
			<Suspense fallback={<div>Loading...</div>}>
				<Switch>
					<Route exact path="/" component={HomeContainer} />
					<Route exact path="/settings" component={SettingsContainer} />
				</Switch>
			</Suspense>
			<ModalRoot />
		</BrowserRouter>
	)
}

export default App

import React from 'react'
import { connect } from 'react-redux'
import UserListItemModal from '../components/blocks/user-list-item-modal'
import { hideModal } from '../actions/index'

const MODAL_COMPONENTS = {
	USER_LIST_ITEM_MODAL: UserListItemModal,
	// other modals
}

const ModalRoot = ({ modalType, modalProps, hideModal }) => {
	if (!modalType) {
		return null
	}

	const SpecificModal = MODAL_COMPONENTS[modalType]
	return <SpecificModal {...modalProps} hideModal={hideModal} />
}

const mapStateToProps = state => ({
	...state.modal,
})

const mapDispatchToProps = dispatch => ({
	hideModal: () => {
		dispatch(hideModal())
	},
})

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(ModalRoot)

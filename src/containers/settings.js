import React from 'react'
import { connect } from 'react-redux'
import Settings from '../components/templates/settings'
import { clearUserState } from '../actions/index'

const SettingsContainer = ({ clearUserState }) => {
	return <Settings clearUserState={clearUserState} />
}

const mapDispatchToProps = dispatch => ({
	clearUserState: () => dispatch(clearUserState()),
})

export default connect(
	null,
	mapDispatchToProps,
)(SettingsContainer)

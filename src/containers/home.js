import React from 'react'
import { connect } from 'react-redux'
import Home from '../components/templates/home'
import { showModal, fetchAllUsers, fetchUsersRequest } from '../actions/index'

const HomeContainer = ({
	showModal,
	users,
	fetchAllUsers,
	isUsersFetching,
	fetchUsersRequest,
	hasMoreUsers,
}) => {
	return (
		<Home
			showModal={showModal}
			users={users}
			hasMoreUsers={hasMoreUsers}
			fetchUsersRequest={fetchUsersRequest}
			fetchAllUsers={fetchAllUsers}
			isUsersFetching={isUsersFetching}
		/>
	)
}

const mapStateToProps = state => ({
	...state.users,
})

const mapDispatchToProps = dispatch => ({
	showModal: (id, modalData, modalType) =>
		dispatch(showModal(id, modalData, modalType)),
	fetchAllUsers: nationality => dispatch(fetchAllUsers(nationality)),
	fetchUsersRequest: isFetching => dispatch(fetchUsersRequest(isFetching)),
})

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(HomeContainer)

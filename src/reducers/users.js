import {
	FETCH_USERS_SUCCESS,
	FETCH_USERS_REQUEST,
	FETCH_USERS_ERROR,
	CLEAR_USER_STATE,
} from '../actions/actionTypes'

const initialState = {
	users: [],
	usersFetchErrorMessage: '',
	isUsersFetching: false,
	hasMoreUsers: false,
}

export default (state = initialState, action) => {
	switch (action.type) {
		case FETCH_USERS_SUCCESS:
			return {
				...state,
				users: state.users.concat(action.users.results),
				hasMoreUsers: state.users.length < 1000,
				isUsersFetching: false,
				usersFetchErrorMessage: '',
			}
		case FETCH_USERS_REQUEST:
			return {
				...state,
				isUsersFetching: action.isUsersFetching,
				usersFetchErrorMessage: '',
			}
		case FETCH_USERS_ERROR:
			return {
				...state,
				usersFetchErrorMessage: action.error,
			}
		case CLEAR_USER_STATE:
			return {
				...state,
				users: [],
			}
		default:
			return state
	}
}

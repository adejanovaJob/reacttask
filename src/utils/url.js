export const setUrlQueryStringToObject = search => {
	return search
		.slice(1)
		.split('&')
		.map(p => p.split('='))
		.reduce((obj, pair) => {
			const [key, value] = pair.map(decodeURIComponent)
			return { ...obj, [key]: value }
		}, {})
}

import reducer from '../../reducers/users'
import * as types from '../../actions/actionTypes'

describe('users reducer', () => {
	it('should return the initial state', () => {
		expect(reducer(undefined, {})).toEqual({
			users: [],
			usersFetchErrorMessage: '',
			isUsersFetching: false,
			hasMoreUsers: false,
		})
	})

	it('should handle FETCH_USERS_REQUEST', () => {
		const fetchUsersRequestAction = {
			type: types.FETCH_USERS_REQUEST,
			isUsersFetching: false,
			usersFetchErrorMessage: '',
		}
		expect(reducer({}, fetchUsersRequestAction)).toEqual({
			isUsersFetching: false,
			usersFetchErrorMessage: '',
		})
	})

	it('should handle CLEAR_USER_STATE', () => {
		const clearUserAction = {
			type: types.CLEAR_USER_STATE,
			users: [],
		}
		expect(reducer({}, clearUserAction)).toEqual({ users: [] })
	})
})

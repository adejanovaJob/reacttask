import React from 'react'
import UserListItem from '../../../components/blocks/user-list-item'
import { shallow } from 'enzyme'

describe('UserListItem', () => {
	const userData = {
		login: {
			uuid: '1234567',
		},
		picture: 'mypicture',
		name: 'Inga',
		email: 'inga.inga95@inbox.lv',
	}

	it('renders UserListItem', () => {
		const props = {
			user: userData,
		}
		const wrapper = shallow(<UserListItem {...props} />)
		expect(wrapper.length).toBe(1)
	})

	it('should call showModal() after clicking of the user list item', () => {
		const props = {
			user: userData,
			showModal: jest.fn(),
		}
		const wrapper = shallow(<UserListItem {...props} />)
		wrapper
			.find('div')
			.at(0)
			.simulate('click')
		expect(props.showModal).toBeCalled()
	})
})

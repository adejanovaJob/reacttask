import * as actions from '../../actions/users'
import * as types from '../../actions/actionTypes'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import moxios from 'moxios'

describe('All user actions', () => {
	const middlewares = [thunk]
	const mockStore = configureMockStore(middlewares)

	it('should create an action - fetchUsersRequest', () => {
		const isUsersFetching = false
		const expectedAction = {
			type: types.FETCH_USERS_REQUEST,
			isUsersFetching,
		}
		expect(actions.fetchUsersRequest(isUsersFetching)).toEqual(expectedAction)
	})

	it('should create an action - clearUserState', () => {
		const expectedAction = {
			type: types.CLEAR_USER_STATE,
		}
		expect(actions.clearUserState()).toEqual(expectedAction)
	})

	describe('fetchAllUsers action', () => {
		beforeEach(() => {
			moxios.install()
		})

		afterEach(() => {
			moxios.uninstall()
		})

		it('successfully fetches users', () => {
			moxios.wait(() => {
				const request = moxios.requests.mostRecent()
				request.respondWith({
					status: 200,
					response: {},
				})
			})

			const expectedActions = [
				{ type: types.FETCH_USERS_REQUEST, isUsersFetching: false },
				{ type: types.FETCH_USERS_SUCCESS, users: {} },
			]

			const store = mockStore({ users: {} })

			return store.dispatch(actions.fetchAllUsers()).then(() => {
				expect(store.getActions()).toEqual(expectedActions)
			})
		})

		it('fails to fetch users', () => {
			moxios.wait(() => {
				const errResp = 'error message'
				const request = moxios.requests.mostRecent()
				request.reject(errResp)
			})

			const expectedActions = [
				{ type: types.FETCH_USERS_REQUEST, isUsersFetching: false },
				{ type: types.FETCH_USERS_ERROR, error: 'error message' },
			]

			const store = mockStore({})

			return store.dispatch(actions.fetchAllUsers()).then(() => {
				expect(store.getActions()).toEqual(expectedActions)
			})
		})
	})
})

import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { useHistory } from 'react-router-dom'

const Settings = ({ clearUserState }) => {
	const nationalities = ['CH', 'ES', 'FR', 'GB']
	const [nationality, setNationality] = useState(nationalities[0])

	const history = useHistory()

	const goBackToHomePage = () => {
		history.push('/')
	}

	const selectNationality = () => {
		clearUserState()
		history.push(`/?nat=${nationality}`)
	}

	return (
		<>
			<button onClick={() => goBackToHomePage()}>Back</button>
			<div>
				<h1>Select nationality of users</h1>
				<select
					value={nationality}
					onChange={e => setNationality(e.target.value)}
				>
					{nationalities.map((item, index) => (
						<option value={item} key={index}>
							{item}
						</option>
					))}
				</select>
				<button onClick={() => selectNationality()}>Select</button>
			</div>
		</>
	)
}

Settings.propTypes = {
	clearUserState: PropTypes.func,
	history: PropTypes.object,
}

export default Settings

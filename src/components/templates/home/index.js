import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import PropTypes from 'prop-types'
import styles from './home.scss'
import UserListItem from '../../blocks/user-list-item'
import { setUrlQueryStringToObject } from '../../../utils/url'

const Home = ({
	showModal,
	fetchAllUsers,
	users,
	isUsersFetching,
	fetchUsersRequest,
	hasMoreUsers,
}) => {
	const [searchTerm, setSearchTerm] = useState('')
	const history = useHistory()

	const handleScroll = () => {
		if (
			window.innerHeight + document.documentElement.scrollTop !==
				document.documentElement.offsetHeight ||
			isUsersFetching
		) {
			return
		}
		fetchUsersRequest(true)
	}

	const searchUrlObject = setUrlQueryStringToObject(history.location.search)
	const nationalityFromSearchUrlObject = searchUrlObject.nat
	const nationalityParameterForUri = nationalityFromSearchUrlObject
		? `nat=${nationalityFromSearchUrlObject}&`
		: ''

	useEffect(() => {
		fetchAllUsers(nationalityParameterForUri)
	}, [])

	useEffect(() => {
		window.addEventListener('scroll', handleScroll)
		return () => window.removeEventListener('scroll', handleScroll)
	}, [])

	useEffect(() => {
		if (!isUsersFetching || !hasMoreUsers) {
			return
		}
		fetchAllUsers(nationalityParameterForUri)
	}, [isUsersFetching])

	let filteredUsers = users
	if (searchTerm) {
		filteredUsers = users.filter(item =>
			`${item.name.first} ${item.name.last}`
				.toLowerCase()
				.startsWith(searchTerm.toLowerCase()),
		)
	}

	const goToSettingsPage = () => {
		history.push('/settings')
	}

	return (
		<>
			<button
				className={styles.settingsButton}
				onClick={() => goToSettingsPage()}
			>
				Go to settings
			</button>
			<div className={styles.searchForm}>
				<label>
					Name, Surname:
					<input
						type="text"
						value={searchTerm}
						onChange={e => setSearchTerm(e.target.value)}
						name="name"
					/>
				</label>
			</div>
			<div className={styles.root}>
				{(filteredUsers || []).map(user => (
					<UserListItem
						showModal={showModal}
						key={user.login.uuid}
						user={user}
					/>
				))}
			</div>
			{isUsersFetching && hasMoreUsers && <div>Loading...</div>}
			{!hasMoreUsers && isUsersFetching && <div>End of users catalog</div>}
		</>
	)
}

Home.propTypes = {
	showModal: PropTypes.func,
	fetchAllUsers: PropTypes.func,
	history: PropTypes.object,
	isUsersFetching: PropTypes.bool,
	fetchUsersRequest: PropTypes.func,
	hasMoreUsers: PropTypes.bool,
	users: PropTypes.array,
}

export default Home

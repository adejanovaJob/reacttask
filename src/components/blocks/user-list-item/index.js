import React from 'react'
import PropTypes from 'prop-types'
import styles from './user-list-item.scss'

const UserListItem = ({ user, showModal }) => {
	const openUserModal = () => {
		showModal(user.uuid, user, 'USER_LIST_ITEM_MODAL')
	}

	const { login, picture, name, email } = user

	return (
		<div
			key={login.username}
			onClick={() => openUserModal()}
			className={styles.root}
		>
			<>
				<img
					alt={login.username}
					src={picture.medium}
					className={styles.image}
				/>
				<div>
					<h2 className={styles.name}>{login.username}</h2>
					<p>{`Name: ${name.first} ${name.last}`}</p>
					<p>Email: {email}</p>
				</div>
			</>
		</div>
	)
}

UserListItem.propTypes = {
	user: PropTypes.object,
	hideModal: PropTypes.func,
}

export default UserListItem

import React from 'react'
import PropTypes from 'prop-types'
import Modal from '../../elements/modal'

const UserListItemModal = ({ hideModal, modalData }) => {
	const { cell, phone, location } = modalData

	return (
		<Modal hideModal={hideModal}>
			<p>Cell: {cell}</p>
			<p>Phone: {phone}</p>
			<p>Street: {location.street.name}</p>
			<p>State: {location.state}</p>
			<p>City: {location.city}</p>
		</Modal>
	)
}

UserListItemModal.propTypes = {
	modalData: PropTypes.object,
	hideModal: PropTypes.func,
}

export default UserListItemModal

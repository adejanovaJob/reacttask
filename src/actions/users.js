import {
	FETCH_USERS_SUCCESS,
	FETCH_USERS_ERROR,
	FETCH_USERS_REQUEST,
	CLEAR_USER_STATE,
} from './actionTypes'
import { BASE_URI } from '../constants'
import axios from 'axios'

export const fetchUsersSuccess = users => {
	return {
		type: FETCH_USERS_SUCCESS,
		users,
	}
}

export const fetchUsersRequest = isUsersFetching => {
	return {
		type: FETCH_USERS_REQUEST,
		isUsersFetching,
	}
}

export const fetchingUsersError = error => {
	return {
		type: FETCH_USERS_ERROR,
		error,
	}
}

export const clearUserState = () => {
	return {
		type: CLEAR_USER_STATE,
	}
}

export const fetchAllUsers = nationality => {
	return dispatch => {
		return axios
			.get(`${BASE_URI}?${nationality}results=50`)
			.then(response => {
				dispatch(fetchUsersRequest(false))
				dispatch(fetchUsersSuccess(response.data))
			})
			.catch(error => {
				dispatch(fetchUsersRequest(false))
				dispatch(fetchingUsersError(error))
			})
	}
}

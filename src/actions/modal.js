import { SHOW_MODAL, HIDE_MODAL } from './actionTypes'

export const hideModal = () => {
	return {
		type: HIDE_MODAL,
	}
}

export const showModal = (id, modalData, modalType) => {
	return dispatch => {
		return dispatch({
			type: SHOW_MODAL,
			modalType,
			modalProps: {
				id,
				modalData,
			},
		})
	}
}

module.exports = {
	entry: {
		main: './src/index.js',
	},
	module: {
		rules: [
			{
				test: /\.(js)$/,
				exclude: /(node_modules)/,
				loader: 'babel-loader',
			},
			{
				test: /\.html$/,
				use: [{ loader: 'html-loader' }],
			},
		],
	},
	optimization: {
		splitChunks: {
			cacheGroups: {
				vendor: {
					test: /[\\/]node_modules[\\/](react|react-dom|axios)[\\/]/,
					name: 'vendors',
					chunks: 'all',
				},
			},
		},
	},
}

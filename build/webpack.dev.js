const path = require('path')
const common = require('./webpack.common')
const merge = require('webpack-merge')
const webpack = require('webpack')
const autoprefixer = require('autoprefixer')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = merge(common, {
	mode: 'development',
	output: {
		filename: '[name].bundle.js',
		path: path.resolve(__dirname, '../dist'),
	},
	devtool: 'inline-source-map',
	devServer: {
		contentBase: './dist',
		noInfo: true,
		hot: true,
		historyApiFallback: true,
	},
	module: {
		rules: [
			{
				test: /\.scss$/,
				use: [
					'style-loader',
					{
						loader: `css-loader`,
						options: {
							importLoaders: 1,
							localIdentName: `[name]__[local]--[hash:base64:5]`,
							sourceMap: true,
						},
					},
					{
						loader: 'postcss-loader',
						options: {
							sourceMap: true,
							plugins: [autoprefixer()],
						},
					},
					{
						loader: 'sass-loader',
						options: {
							sourceMap: true,
							includePaths: [
								path.resolve('src/styles'),
								path.resolve('src/components'),
								path.resolve('node_modules'),
							],
						},
					},
				],
				include: path.resolve('src'),
			},
		],
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new HtmlWebpackPlugin({
			template: './src/index.html',
			filename: 'index.html',
		}),
	],
})
